import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { playVideo } from 'app/ducks/videos';
import styles from './index.module.scss';

class VideoPlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidUpdate(prevProps) {
    const { currentVideoUrl, currentVideo, onlyVideo } = this.props;
    if (!onlyVideo && currentVideo !== prevProps.currentVideo) {
      this.playNextVideo(currentVideoUrl);
    }
  }

  onEndedVideo = () => {
    const { onlyVideo, currentVideo, totalVideos, setVideoToPlay } = this.props;
    if (onlyVideo) this.video.play();
    else {
      const countUp = currentVideo + 1;
      const nextVideo = countUp === totalVideos ? 0 : countUp;
      setVideoToPlay(nextVideo);
    }
  };

  playNextVideo(url) {
    this.video.src = url;
    this.video.load();
    this.video.play();
  }

  render() {
    const { currentVideoUrl, totalVideos } = this.props;
    return (
      <div className={styles.player}>
        <video
          className={styles.video}
          autoPlay
          muted
          controls={totalVideos > 0}
          ref={video => {
            this.video = video;
          }}
          onEnded={this.onEndedVideo}
          src={currentVideoUrl || undefined}
        >
          {`Your browser doesn't support this videoplayer.`}
        </video>
      </div>
    );
  }
}

const mapStateToProps = ({ videos }) => ({
  onlyVideo: videos.videos.length === 1,
  currentVideo: videos.current,
  totalVideos: videos.videos.length,
  currentVideoUrl:
    videos.videos.length > 0 ? videos.videos[videos.current].url : '',
});

const mapDispatchToProps = {
  setVideoToPlay: playVideo,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoPlayer);

VideoPlayer.propTypes = {
  currentVideoUrl: PropTypes.string.isRequired,
  totalVideos: PropTypes.number.isRequired,
  setVideoToPlay: PropTypes.func.isRequired,
  onlyVideo: PropTypes.bool.isRequired,
  currentVideo: PropTypes.number,
};

VideoPlayer.defaultProps = {
  currentVideo: null,
};
