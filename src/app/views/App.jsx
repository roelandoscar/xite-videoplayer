import React from 'react';

import VideoPlayer from './components/VideoPlayer';
import Form from './components/Form';
import PlayList from './components/PlayList';

import styles from './App.module.scss';

const App = () => (
  <main className={styles.main}>
    <VideoPlayer />
    <Form />
    <PlayList />
  </main>
);

export default App;
