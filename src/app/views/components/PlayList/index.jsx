import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { playVideo, toggleVideoList } from 'app/ducks/videos';
import styles from './index.module.scss';

class PlayList extends Component {
  constructor(props) {
    super(props);

    this.state = { hide: true };
  }

  componentWillReceiveProps(nextProps) {
    const { videos } = nextProps;
    const { hide } = this.state;

    // show playlist header after first added video
    if (videos.length > 0 && hide) {
      this.setState({ hide: false });
    }
  }

  selectVideo = id => {
    const { setVideoToPlay, videos } = this.props;

    // extra check based on id of the video to make sure we select the correct video
    const selectedIndex = videos.findIndex(vid => vid.id === id);
    setVideoToPlay(selectedIndex);
  };

  togglePlaylist = () => {
    const { setVideoListToggle } = this.props;
    setVideoListToggle();
  };

  render() {
    const { videos, showVideoList } = this.props;
    const { hide } = this.state;
    return (
      <div className={[styles.playlist, hide && styles.hide].join(' ')}>
        <header className={styles.header}>
          <h2>
            <span className={styles.count}>{videos.length}</span>
            {`video${videos.length > 1 ? `'s` : ``} added`}
          </h2>
          <button
            type="button"
            className={styles.toggle}
            onClick={this.togglePlaylist}
          >
            {showVideoList ? 'hide' : 'show'}
          </button>
        </header>
        <ul
          className={[styles.video_list, showVideoList ? styles.open : ''].join(
            ' '
          )}
        >
          {videos.map(video => (
            <li
              key={`video-${video.id}`}
              className={styles.video}
              title={video.url}
            >
              <button type="button" onClick={() => this.selectVideo(video.id)}>
                {video.url}
              </button>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  videos: state.videos.videos,
  showVideoList: state.videos.showList,
  currentVideo: state.videos.currentVideo,
});

const mapDispatchToProps = {
  setVideoToPlay: playVideo,
  setVideoListToggle: toggleVideoList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayList);

PlayList.propTypes = {
  videos: PropTypes.instanceOf(Array).isRequired,
  setVideoToPlay: PropTypes.func.isRequired,
  setVideoListToggle: PropTypes.func.isRequired,
  showVideoList: PropTypes.bool.isRequired,
};
