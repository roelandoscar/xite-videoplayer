import createAction from '../services/createAction';

const VIDEO_ADDED = 'VIDEO_ADDED';
const VIDEO_PLAY = 'VIDEO_PLAY';
const VIDEOLIST_TOGGLE = 'VIDEOLIST_TOGGLE';

const initialState = {
  current: null,
  loading: false,
  videos: [],
  showList: false,
};

export const videos = (state = initialState, { type, payload }) => {
  const current = state.current || 0;
  switch (type) {
    case VIDEO_ADDED:
      return {
        ...state,
        videos: [
          ...state.videos,
          {
            url: payload,
            id: state.videos.length,
          },
        ],
        current,
      };
    case VIDEO_PLAY:
      return {
        ...state,
        current: payload,
      };
    case VIDEOLIST_TOGGLE:
      return {
        ...state,
        showList: !state.showList,
      };
    default:
      return state;
  }
};

export const videoAdded = createAction(VIDEO_ADDED);
export const videoPlay = createAction(VIDEO_PLAY);
export const videoListToggled = createAction(VIDEOLIST_TOGGLE);

export const addVideo = video => dispatch => {
  dispatch(videoAdded(video));
};
export const playVideo = index => dispatch => {
  dispatch(videoPlay(index));
};
export const toggleVideoList = () => dispatch => {
  dispatch(videoListToggled());
};
