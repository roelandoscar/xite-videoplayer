import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';

import { addVideo } from 'app/ducks/videos';
import testVideo from './testVideo';

import styles from './index.module.scss';

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      url: '',
      error: false,
      status: 'idle',
      layout: 'center',
      hide: false,
    };

    this.feedback = {
      idle: '',
      checking: 'Checking video file',
      error: `This video url doesn't work`,
      done: 'Great! Video is added.',
    };
  }

  handleUrlChange = event => {
    event.preventDefault();

    this.setState({ url: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ status: 'checking' });

    setTimeout(() => {
      const { url } = this.state;

      // Test if video can play through before adding it to the playlist
      testVideo(url)
        .then(() => {
          this.setState({ url: '', status: 'done', hide: true });
          const { setVideoToAdd } = this.props;
          const { layout } = this.state;

          // Timeout to allow messages to show
          setTimeout(() => {
            setVideoToAdd(url);
          }, 1000);

          // Change layout to corner after first added video
          if (layout !== 'corner') {
            setTimeout(() => {
              this.setState({ layout: 'corner', hide: false });
            }, 1500);
          }
        })
        .catch(() => {
          this.setState({
            error: true,
            status: 'error',
          });
          // Automatically reset after error
          setTimeout(() => {
            this.setState({ status: 'idle' });
          }, 1500);
        });
    }, 1000);
  };

  render() {
    const { url, error, status, layout, hide } = this.state;
    const { videosTotal, showVideoList } = this.props;
    let cornerToggleStyle = '';
    if (layout === 'corner') {
      cornerToggleStyle = !showVideoList
        ? styles[`form--hide`]
        : styles[`form--show`];
    }
    const centerToggleStyle =
      layout === 'center' && hide ? styles[`form--center-hide`] : '';
    return (
      <form
        onSubmit={this.handleSubmit}
        className={[
          styles.form,
          styles[`form--${layout}`],
          cornerToggleStyle,
          centerToggleStyle,
        ].join(' ')}
      >
        {videosTotal === 0 && (
          <h1 className={styles.title}>
            Enter the url of a videofile to start
          </h1>
        )}
        <div className={[styles.fields, styles[`fields--${status}`]].join(' ')}>
          <div className={styles.inputsholder}>
            <input
              id="url"
              type="url"
              value={url}
              onChange={this.handleUrlChange}
              className={styles.input}
              placeholder={
                layout === 'corner' ? 'Add another video' : undefined
              }
            />
            <button
              type="submit"
              className={styles.submit}
              disabled={!['idle', 'done'].includes(status)}
            >
              GO
            </button>
          </div>
          <div className={styles.feedback}>{this.feedback[status]}</div>
        </div>
        {error.state && <span>{error.message}</span>}
      </form>
    );
  }
}

const mapStateToProps = state => ({
  videosTotal: state.videos.videos.length,
  showVideoList: state.videos.showList,
});

const mapDispatchToProps = {
  setVideoToAdd: addVideo,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);

Form.propTypes = {
  setVideoToAdd: PropTypes.func.isRequired,
  videosTotal: PropTypes.number.isRequired,
  showVideoList: PropTypes.bool.isRequired,
};
