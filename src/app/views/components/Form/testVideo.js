const testVideo = url =>
  new Promise((resolve, reject) => {
    const video = document.createElement('video');

    video.oncanplaythrough = () => {
      resolve(true);
    };

    video.onerror = () => {
      reject();
    };

    video.src = url;
  });

export default testVideo;
