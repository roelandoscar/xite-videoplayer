# Xite Coding Challenge

Implement video player with ability to add videos.

## Execution

To start the project I used Create React App to have a base. Set up eslint to lint with eslint-config-airbnb to have better linting config. For styling stylelint is used. To prevent commits with lint errors I used husky and lint-staged to check actual commit.

For styling and the logic of the videoplayer I didn't use any libraries or package but wanted to write it myself.

The app is divided in three main components, Form, PlayList, VideoPlayer. They are connected to the store to used the state, functions from the store and to be able to interaction with each other.

To run the project locally run `npm start`  
To build the project run `npm run build`

The project is also hosted on https://xite-videoplayer.firebaseapp.com (turnoff browser plugins which can prevent loading the video file because of CORS errors)
